# Sticky Notes

Winform 으로 만든 간단한 StickyNotes 프로그램이며, Windows 와 .NET Framework 환경에서 동작합니다.

## Build

* [VisualStudio](https://visualstudio.microsoft.com)

## Authors

CH Kim

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
