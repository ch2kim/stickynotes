﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace StickyNote
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool createdNew;
            Mutex mutex = new Mutex(true, Application.ProductName, out createdNew);
            if (!createdNew)
            {
                MessageBox.Show("프로그램이 이미 실행되고 있습니다.");
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //var mainform = new InputTextDialog();
            var mainform = new MainForm();
            Application.Run(mainform);  // 숨겨진 form 을 이용해서 메모창을 띄운다.

            mutex.ReleaseMutex();
            mutex = null;
        }
    }
}
