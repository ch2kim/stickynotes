﻿namespace StickyNote
{
    partial class MemoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.titleLabel = new System.Windows.Forms.Label();
            this.closeButton = new System.Windows.Forms.Button();
            this.readOnlyButton = new System.Windows.Forms.Button();
            this.memoBox = new StickyNote.Controls.AlphaRichTextBox();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.BackColor = System.Drawing.SystemColors.Window;
            this.titleLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.titleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleLabel.Font = new System.Drawing.Font("Malgun Gothic", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleLabel.Location = new System.Drawing.Point(2, 2);
            this.titleLabel.Margin = new System.Windows.Forms.Padding(1);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Padding = new System.Windows.Forms.Padding(1);
            this.titleLabel.Size = new System.Drawing.Size(196, 20);
            this.titleLabel.TabIndex = 0;
            this.titleLabel.Text = "Title";
            this.titleLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.titleLabel_MouseDown);
            this.titleLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.titleLabel_MouseMove);
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.BackColor = System.Drawing.Color.DarkRed;
            this.closeButton.FlatAppearance.BorderSize = 0;
            this.closeButton.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.closeButton.Location = new System.Drawing.Point(178, 4);
            this.closeButton.Margin = new System.Windows.Forms.Padding(0);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(16, 16);
            this.closeButton.TabIndex = 3;
            this.closeButton.UseVisualStyleBackColor = false;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // readOnlyButton
            // 
            this.readOnlyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.readOnlyButton.BackColor = System.Drawing.Color.Transparent;
            this.readOnlyButton.FlatAppearance.BorderSize = 0;
            this.readOnlyButton.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.readOnlyButton.ForeColor = System.Drawing.Color.Transparent;
            this.readOnlyButton.Location = new System.Drawing.Point(154, 4);
            this.readOnlyButton.Margin = new System.Windows.Forms.Padding(0);
            this.readOnlyButton.Name = "readOnlyButton";
            this.readOnlyButton.Size = new System.Drawing.Size(16, 16);
            this.readOnlyButton.TabIndex = 2;
            this.readOnlyButton.Text = "R";
            this.readOnlyButton.UseVisualStyleBackColor = false;
            this.readOnlyButton.Click += new System.EventHandler(this.readOnlyButton_Click);
            // 
            // memoBox
            // 
            this.memoBox.AcceptsTab = true;
            this.memoBox.BackColor = System.Drawing.SystemColors.Window;
            this.memoBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.memoBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoBox.Location = new System.Drawing.Point(2, 22);
            this.memoBox.Margin = new System.Windows.Forms.Padding(0);
            this.memoBox.Multiline = true;
            this.memoBox.Name = "memoBox";
            this.memoBox.Size = new System.Drawing.Size(196, 176);
            this.memoBox.TabIndex = 1;
            this.memoBox.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.memoBox_PreviewKeyDown);
            // 
            // MemoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(200, 200);
            this.Controls.Add(this.memoBox);
            this.Controls.Add(this.readOnlyButton);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.titleLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MemoForm";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Memo";
            this.Deactivate += new System.EventHandler(this.MemoForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MemoForm_FormClosing);
            this.Load += new System.EventHandler(this.MemoForm_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.memoForm_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.memoForm_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.memoForm_MouseUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Button readOnlyButton;
        // private System.Windows.Forms.RichTextBox memoBox;
        private StickyNote.Controls.AlphaRichTextBox memoBox;
    }
}

