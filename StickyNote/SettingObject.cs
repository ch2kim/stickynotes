﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Windows.Forms;

namespace StickyNote
{
    // 참고 사이트
    /* https://www.codeproject.com/Articles/22717/Using-PropertyGrid
     * https://stackoverflow.com/questions/24545905/propertygrid-customizing-font-property
     * https://stackoverflow.com/questions/884948/how-to-set-propertygrid-griditem-tag
     */
    class MyFontEditor : UITypeEditor
    {
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            FontDialog dlg = new FontDialog();
            dlg = new FontDialog();

            Font font = value as Font;
            if (font != null)
            {
                dlg.Font = font;
            }

            if (dlg.ShowDialog() == DialogResult.OK)
                return dlg.Font;

            return base.EditValue(context, provider, value);
        }
    }
    public class MyImagePathLocator : UITypeEditor
    {
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            // Show the dialog we use to open the file.
            // You could use a custom one at this point to provide the file path and the image.
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Image Files(*.bmp;*.jpg)|*.bmp;*.jpg|All files (*.*)|*.*";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Bitmap bitmap = new Bitmap(openFileDialog.FileName);
                    // bitmap.SetPath(openFileDialog.FileName);

                    return openFileDialog.FileName;
                }
            }

            return value;
        }

        /*
        public override bool GetPaintValueSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override void PaintValue(PaintValueEventArgs e)
        {
            if (e.Value != null)
            {
                // Get image
                Bitmap bmp = (Bitmap)e.Value;

                // This rectangle indicates the area of the Properties window to draw a representation of the value within.
                Rectangle destRect = e.Bounds;

                // Optional to set the default transparent color transparent for this image.
                bmp.MakeTransparent();

                // Draw image
                e.Graphics.DrawImage(bmp, destRect);
            }
        }
        */
    }

    class SettingObject
    {
        private Single opacity;

        [Category("Memo")]
        public Size WindowSize { get; set; }
        [Category("Memo")]
        public Color BackgroundColor { get; set; }
        [Category("Memo")]
        public Color TextColor { get; set; }
        [Category("Memo")]
        [Editor(typeof(MyFontEditor), typeof(UITypeEditor))]
        public Font TextFont { get; set; }
        [Category("Memo")]
        public Single Opacity { get => opacity; set { opacity = value > 1 ? 1 : value < 0 ? 0 : value; } }

        //[Category("Memo")]
        //[Editor(typeof(MyImagePathLocator), typeof(UITypeEditor))]
        //public string BackgroundImage { get; set; }

        [Category("App")]
        [Editor(typeof(MyFontEditor), typeof(UITypeEditor))]
        public Font AppFont { get; set; }

        [Category("System")]
        public string Prefix { get; set; }

        // TODO : Autosave 구현
        //[Category("App")]
        //public int AutoSaveTime { get; set; }
    }
}
