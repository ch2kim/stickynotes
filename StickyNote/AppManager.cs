﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace StickyNote
{
    /*
     * (o) POPUP 메모 윈도우 를 만들고, Drag 로 이동(move) / 크기조정(resize) 구현
     * (o) 설정 윈도우를 만들고, json 포맷으로 설정을 저장한다.
     * (o) Dictionary 를 사용하여 메모 목록을 저장한다. Key 는 생성날짜 + Tick 끝자리 등으로 만든다.
     * (o) 보이지 않는 form 을 사용해야 한다.
     * (o) 수정시간 적용
     * (o) 제목 정하기
     * TODO : 메모 목록을 파일로 저장한다.
     * TODO : 일단 메뉴 한글화
     * TODO : 설정 폼에 저장 버튼 만듦.
     * TODO : 메모 배경 추가 가능
     */

    class AppManager
    {
        /*
         * Config 관리
         * 열려있는 Memo 관리 및 MemoForm 에 대한 관리
         * 설정 변경시 Form 등 각 컨트롤에 이벤트 보내기
         */

        public static AppManager Instance { get; private set; }

        // Config Object
        public SettingObject settingObject = null;
        public Bitmap memoBackgroudImage = null;

        // Config, Path
        private string configPath;
        private string configFilename;
        private string userFilename;

        private string dataPath;
        
        public Form MainWindow { get; private set; }
        public AppManager()
        {
            Instance = this;
        }

        public void Init(Form mainWindow)
        {
            MainWindow = mainWindow;

            string currentDirectory = Directory.GetCurrentDirectory();

            configPath = string.Format("{0}\\config\\", currentDirectory);
            configFilename = configPath + "stickynote.config.json";
            userFilename = configPath + "user.config.json";

            dataPath = string.Format("{0}\\data\\", currentDirectory);

            if (!LoadSettingObject())
            {
                Directory.CreateDirectory(configPath);

                settingObject = createDefaultSettingObject();
                SaveSettingObject();
            }

            if (!LoadUserObject())
            {
                Directory.CreateDirectory(configPath);

                userInfo = new UserInfo();
                SaveUserObject();
            }

            UpdateBackgroundImage();

            Directory.CreateDirectory(dataPath);
            LoadMemoDatasFromFile();
        }

        public void Quit()
        {
            SaveAllMemoDatesToFile();
            CloseAllMemoWindow();

            CloseSettingWindow();
            CloseListWindow();

            SaveSettingObject();
            SaveUserObject();

            Instance = null;
        }

        private SettingObject createDefaultSettingObject()
        {
            var s = new SettingObject();
            s.BackgroundColor = Color.Yellow;
            s.TextColor = Color.Black;
            s.WindowSize = new Size(500, 380);
            s.Opacity = 1.0f;

            // s.AutoSaveTime = 1 * 60;    // 1 분에 한번이 기본
            Font font = new Font(new FontFamily("Arial"), 10.0f, FontStyle.Regular);
            s.TextFont = font;

            return s;
        }
  
        private static bool LoadObjectFromJson<T>(string filename, ref T target)
        {
            if (File.Exists(filename))
            {
                string jsonText = File.ReadAllText(filename);
                T obj = JsonConvert.DeserializeObject<T>(jsonText);

                if (obj != null)
                {
                    target = obj;
                    return true;
                }
            }

            return false;
        }

        private static bool SaveObjectToJson<T>(string filename, T target)
        {
            if (target != null)
            {
                string jsonText = JsonConvert.SerializeObject(target, Formatting.Indented);
                if (jsonText != null)
                {
                    File.WriteAllText(filename, jsonText);
                    return true;
                }
            }

            return false;
        }

        private bool LoadSettingObject()
        {
            return LoadObjectFromJson<SettingObject>(configFilename, ref settingObject);
        }

        private void SaveSettingObject()
        {
            SaveObjectToJson<SettingObject>(/*configPath, */configFilename, settingObject);
        }

        private bool LoadUserObject()
        {
            return LoadObjectFromJson<UserInfo>(userFilename, ref userInfo);
        }

        private void SaveUserObject()
        {
            SaveObjectToJson<UserInfo>(/*configPath, */userFilename, userInfo);
        }

        class UtilWindowInfo
        {
            public Point LastedLocation;
            public Size LastedSize;
            public Form WindowForm;
        }

        class UserInfo
        {
            public Point LastedFormLocation = new Point(-1, -1);
            public UtilWindowInfo settingWinInfo = null;
            public UtilWindowInfo listWinInfo = null;
        }

        // Forms
        private UserInfo userInfo = null;

        private void ShowUtilWindow<T>(ref UtilWindowInfo wi) where T : Form, new()
        {
            if (wi == null)
            {
                wi = new UtilWindowInfo();
                
                T f = new T();
                wi.LastedLocation = f.Location;
                wi.LastedSize = f.Size;
                wi.WindowForm = f;
            }

            if (wi.WindowForm == null)
            {
                T f = new T();
                f.Location = wi.LastedLocation;
                f.Size = wi.LastedSize;
                wi.WindowForm = f;
            }

            UpdateSettingUtilWindow(ref wi);

            var wf = wi.WindowForm;
            wf.Show();
            wf.Focus();
        }

        private void CloseUtilWindow(ref UtilWindowInfo wi)
        {
            if (wi != null)
            {
                var f = wi.WindowForm;
                if (f != null)
                {
                    wi.LastedLocation = f.Location;
                    wi.LastedSize = f.Size;
                    wi.WindowForm = null;
                }
            }
        }

        private void UpdateSettingUtilWindow(ref UtilWindowInfo wi)
        {
            if (wi != null)
            {
                var f = wi.WindowForm; // as IUtilForm;
                if (f != null)
                {
                    var appFont = settingObject.AppFont;
                    if (appFont != null)
                        f.Font = appFont;
                }
            }
        }

        public void ShowSettingWindow()
        {
            ShowUtilWindow<SettingForm>(ref userInfo.settingWinInfo);
        }

        public void ShowListWindow()
        {
            ShowUtilWindow<ListForm>(ref userInfo.listWinInfo);
        }

        public void CloseSettingWindow()
        {
            CloseUtilWindow(ref userInfo.settingWinInfo);
        }

        public void CloseListWindow()
        {
            CloseUtilWindow(ref userInfo.listWinInfo);
        }

        // Forms
        public Dictionary<string, MyMemoData> MemoTable
        {
            get; private set;
        } = new Dictionary<string, MyMemoData>();

        static object locker = new object();
        static DateTime lastedKeyTime = DateTime.UtcNow;

        private void LoadMemoDatasFromFile()
        {
            if (!File.Exists(dataPath))
                Directory.CreateDirectory(dataPath);

            string[] memoFiles = Directory.GetFiles(dataPath, "*.memo");
            foreach(var memoFile in memoFiles)
            {
                var memoData = LoadMemoData(memoFile);
                if (memoData != null)
                {
                    if (memoData.show)
                        ShowMemoWindow(memoData.key, false);
                }
            }
        }

        private MyMemoData LoadMemoData(string filename)
        {
            MyMemoData memoData = null;
            if (LoadObjectFromJson<MyMemoData>(filename, ref memoData))
            {
                Debug.Assert(memoData != null && !MemoTable.ContainsKey(memoData.key));
                MemoTable.Add(memoData.key, memoData);

                return memoData;
            }

            return null;
        }

        private void SaveAllMemoDatesToFile()
        {
            foreach(var m in MemoTable)
            {
                SaveMemoData(m.Key, m.Value);
            }
        }

        public void SaveMemoDataFromKey(string key)
        {
            MyMemoData memoData = null;
            if (MemoTable.TryGetValue(key, out memoData))
            {
                SaveMemoData(key, memoData);
            }
        }

        private void SaveMemoData(string key, MyMemoData memoData)
        {
            if (memoData.saved == false)
            {
                Debug.Assert(key.Equals(memoData.key));

                var filename = string.Format("{0}{1}.memo", dataPath, key);
                if (SaveObjectToJson<MyMemoData>(filename, memoData))
                    memoData.saved = true;
            }
        }

        static string Generate15UniqueDigits(string prefix)
        {
            // stack overflow 참고
            lock (locker)
            {
                DateTime keyTime;
                while((keyTime = DateTime.Now).Equals(lastedKeyTime))
                {
                    Thread.Sleep(50);
                };

                lastedKeyTime = keyTime;
                return string.Format("{0}.{1}", prefix, lastedKeyTime.ToString("yyyyMMddHHmmssf"));
            }
        }
        public Form CreateNewMemoWindow()
        {
            var prefix = settingObject.Prefix;
            var key = Generate15UniqueDigits(string.IsNullOrEmpty(prefix) ? "_key" : prefix);

            userInfo.LastedFormLocation.X += 20;
            userInfo.LastedFormLocation.Y += 20;
            
            var memoData = new MyMemoData();
            memoData.key = key;
            memoData.memoLocation = userInfo.LastedFormLocation;
            memoData.memoSize = settingObject.WindowSize;
            memoData.memoState = FormWindowState.Normal;
            memoData.createdTime = DateTime.Now;
            memoData.modifiedTime = memoData.createdTime;
            memoData.show = true;

            var form = new MemoForm();
            memoData.memoForm = form;
            form.MemoData = memoData;

            form.TitleText = memoData.createdTime.ToDateString();
            form.MemoText = string.Empty;
            form.Location = memoData.memoLocation;
            form.Size = memoData.memoSize;

            form.Show();

            MemoTable.Add(key, memoData);          
            AddMemoList(memoData);

            UpdateMemoWindow(memoData);

            return form;
        }

        public void ShowMemoWindow(string key, bool beFocus)
        {
            MyMemoData memoData;
            if (MemoTable.TryGetValue(key, out memoData))
            {
                if (memoData.memoForm != null)
                {
                    MemoForm f = memoData.memoForm;
                    f.Show();

                    if (beFocus)
                        f.Focus();
                }
                else
                {
                    // TODO : MemoData 가 셋 될 때 자동으로 넣어주면 어떨까?
                    MemoForm form = new MemoForm();
                    memoData.memoForm = form;
                    memoData.show = true;

                    form.MemoData = memoData;

                    form.Location = memoData.memoLocation;
                    form.Size = memoData.memoSize;
                    form.WindowState = memoData.memoState;
                    form.TitleText = memoData.titleText;
                    form.MemoText = memoData.memoText;

                    form.Show();

                }

                UpdateMemoWindow(memoData);
            }
        }
        public void MemoFormDeactive(MemoForm form)
        {
            userInfo.LastedFormLocation = form.Location;
        }

        public void ClosingMemoWindow(MyMemoData memoData)
        {
            if (memoData != null)
            {
                var form = memoData.memoForm;
                if (form != null)
                {
                    memoData.memoLocation = form.Location;
                    memoData.memoSize = form.Size;
                    memoData.memoState = form.WindowState;
                    memoData.titleText = form.TitleText;
                    memoData.memoText = form.MemoText;
                    memoData.show = false;
                    memoData.memoForm = null;

                    form.MemoData = null;
                }
            }
        }

        public void CloseMemoWindow(String key)
        {
            MyMemoData memoData = null;
            if (MemoTable.TryGetValue(key, out memoData))
            {
                var form = memoData.memoForm;
                if (form != null)
                {
                    form.Close();
                }
            }
        }

        public void UpdateMemoWindow(MyMemoData memoData)
        {
            Debug.Assert(memoData != null);
            if (memoData.memoForm != null)
            {
                var f = memoData.memoForm;
                f.SetEnableModified(false);

                memoData.memoForm.Invalidate();

                f.SetTextFont(settingObject.TextFont);
                f.SetTextColor(settingObject.TextColor);
                f.SetBackgroundColor(settingObject.BackgroundColor);

                f.Opacity = settingObject.Opacity;

                f.SetEnableModified(true);
            }
        }

        public void RemoveMemo(string key, bool listWidget)
        {
            CloseMemoWindow(key);
            MemoTable.Remove(key);

            if (listWidget)
                RemoveMemoList(key);
        }

        public void CloseAllMemoWindow()
        {
            if (MemoTable == null
                || MemoTable.Count == 0)
                return;

            string[] keys = new string[MemoTable.Count];
            MemoTable.Keys.CopyTo(keys, 0);

            foreach(var k in keys)
                CloseMemoWindow(k);
        }

        public void AddMemoList(MyMemoData d)
        {
            var listWinInfo = userInfo.listWinInfo;
            ListForm listForm = listWinInfo != null ? listWinInfo.WindowForm as ListForm : null;
            if (listForm != null)
                listForm.AddMemoListItem(d);
        }

        public void RemoveMemoList(string key)
        {
            var listWinInfo = userInfo.listWinInfo;
            ListForm listForm = listWinInfo != null ? listWinInfo.WindowForm as ListForm : null;
            if (listForm != null)
                listForm.RemoveMemoListItem(key);
        }

        public void UpdateMemoList(MyMemoData d)
        {
            var listWinInfo = userInfo.listWinInfo;
            ListForm listForm = listWinInfo != null ? listWinInfo.WindowForm as ListForm : null;
            if (listForm != null)
                listForm.UpdateDataListItem(d);
        }

        public void OnAppSettingChanged()
        {
            UpdateSettingUtilWindow(ref userInfo.settingWinInfo);
            UpdateSettingUtilWindow(ref userInfo.listWinInfo);
        }

        public void UpdateBackgroundImage()
        {
            //if (string.IsNullOrEmpty(settingObject.BackgroundImage))
            //{
            //    memoBackgroudImage = null;
            //}
            //else
            //{
            //    memoBackgroudImage = new Bitmap(settingObject.BackgroundImage);
            //}

            System.GC.Collect();
        }

        public void OnMemoSettingChanged()
        {
            UpdateBackgroundImage();

            foreach (var v in MemoTable.Values)
            {
                UpdateMemoWindow(v);
            }
        }
    }
}
