﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace StickyNote
{
    public partial class SettingForm : Form
    {
        public SettingForm()
        {
            InitializeComponent();
        }

        private void SettingForm_Load(object sender, EventArgs e)
        {
            var am = AppManager.Instance;
            if (am != null)
                settingPropertyGrid.SelectedObject = am.settingObject;
        }

        private void SettingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            var am = AppManager.Instance;
            if (am != null)
            {
                am.CloseSettingWindow();
            }
        }

        private void settingPropertyGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            string category = null;
            GridItem item = e.ChangedItem;

            while (item.Parent != null)
            {
                if (item.Parent.Label.Contains("SettingObject"))
                {
                    category = item.Label;
                    break;
                }

                item = item.Parent;
            }

            if (category.Equals("app", StringComparison.OrdinalIgnoreCase))
            {
                var am = AppManager.Instance;
                Debug.Assert(am != null);

                am.OnAppSettingChanged();
            }
            else if (category.Equals("memo", StringComparison.OrdinalIgnoreCase))
            {
                var am = AppManager.Instance;
                Debug.Assert(am != null);

                am.OnMemoSettingChanged();
            }
        }
    }
}
