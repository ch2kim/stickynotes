﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace StickyNote
{
    public partial class MemoForm : Form
    {
        public MemoForm()
        {
            InitializeComponent();
            //memoBox.Rtf = rtf.Replace("ansicpg949", "ansicpg1201");
        }

        public string TitleText
        {
            get { return titleLabel.Text; }
            set 
            { 
                titleLabel.Text = value;
                memoData.titleText = titleLabel.Text;
            }
        }

        public string MemoText
        {
            get { return memoBox.Text; }
            set 
            { 
                memoBox.Text = value;
                memoData.memoText = memoBox.Text;
            }
        }

        public void SetTextFont(Font font)
        {
            memoBox.Font = font;
        }

        public void SetTextColor(Color color)
        {
            memoBox.ForeColor = color;
        }
        public void SetBackgroundColor(Color backgroundColor)
        {
            //this.BackColor;
            memoBox.BackColor = backgroundColor;
        }

        private bool enableModified = false;
        public void SetEnableModified(bool v)
        {
            enableModified = v;
        }

        private MyMemoData memoData = null;
        public MyMemoData MemoData { get => memoData; set => memoData = value; }

        private Point titleBarMouseDownLocation = new Point();
        private Point memoBoxMouseDownLocation = new Point();
        
        private uint formResizeFlag = 0x00;
        private bool moveState = false;

        private void MemoForm_Load(object sender, EventArgs e)
        {
            var titleContextMenu = new ContextMenu();
            titleContextMenu.MenuItems.Add("&Edit Title", OnEditTitle);
            
            titleLabel.ContextMenu = titleContextMenu;

            memoBox.TextChanged += memoBox_TextChanged;
            //memoBox.OnPaintHandle = OnPaintTextBox;
        }

        private void OnEditTitle(object sender, EventArgs e)
        {
            InputTextDialog dialog = new InputTextDialog();
            dialog.InputText = TitleText;

            var dr = dialog.ShowDialog();
            if (dr == DialogResult.OK)
            {
                TitleText = dialog.InputText;

                var am = AppManager.Instance;
                Debug.Assert(am != null);
                am.UpdateMemoList(memoData);
            }
        }

        private void titleLabel_MouseDown(object sender, MouseEventArgs e)
        {
            titleBarMouseDownLocation = new Point(e.X, e.Y);
        }

        private void titleLabel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var formLoc = Location;
                int dx = e.X - titleBarMouseDownLocation.X;
                int dy = e.Y - titleBarMouseDownLocation.Y;

                formLoc.X += dx;
                formLoc.Y += dy;

                Location = formLoc;
            }
        }

        // TODO : Left,Top 쪽을 드래그 해서 크기 조절하기
        const uint RESIZE_WIDTH = 0x01;
        const uint RESIZE_HEIGHT = 0x02;
        const uint RESIZE_WITHMOVE = 0x04;
        const uint RESIZE_WH = RESIZE_WIDTH | RESIZE_HEIGHT;

        private uint GetCursorTypeFromLocation(Control c, Point mousePoint, ref Cursor mouseCursor)
        {
            int gap = 2;
            int x = mousePoint.X;
            int y = mousePoint.Y;
            int dx = Math.Abs(c.Width - x);
            int dy = Math.Abs(c.Height - y);

            uint flag = 0x00;
            if (x < gap)
            {
                //flag |= RESIZE_WIDTH | RESIZE_WITHMOVE;
            }
            else if (dx <= gap)
            {
                flag |= RESIZE_WIDTH;
            }
            if (y < gap)
            {
                //flag |= RESIZE_HEIGHT | RESIZE_WITHMOVE;
            }
            else if (dy <= gap)
            {
                flag |= RESIZE_HEIGHT;
            }

            if ((flag & RESIZE_WH) == RESIZE_WH)
            {
                mouseCursor = Cursors.SizeNWSE;
            }
            else if ((flag & RESIZE_WIDTH) == RESIZE_WIDTH)
            {
                mouseCursor = Cursors.SizeWE;
            }
            else if ((flag & RESIZE_HEIGHT) == RESIZE_HEIGHT)
            {
                mouseCursor = Cursors.SizeNS;
            }

            return flag;
        }

        private void memoForm_MouseDown(object sender, MouseEventArgs e)
        {
            memoBoxMouseDownLocation = Cursor.Position;

            Cursor cursorType = Cursor.Current;
            uint resizeFlag = GetCursorTypeFromLocation(this, e.Location, ref cursorType);

            if (Cursor.Current != cursorType)
                Cursor.Current = cursorType;

            formResizeFlag = resizeFlag;
            if (resizeFlag != 0x00)
                moveState = true;
        }

        private void memoForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (formResizeFlag != 0x00 && moveState)
            {
                var location = Cursor.Position;

                int dx = 0;
                int dy = 0;

                if ((formResizeFlag & RESIZE_WIDTH) != 0)
                {
                    dx = location.X - memoBoxMouseDownLocation.X;
                }
                if ((formResizeFlag & RESIZE_HEIGHT) != 0)
                {
                    dy = location.Y - memoBoxMouseDownLocation.Y;
                }

                var formLocation = this.Location;

                if ((formResizeFlag & RESIZE_WITHMOVE) != 0)
                {

                    formLocation.X += dx;
                    formLocation.Y += dy;

                    dx = -dx;
                    dy = -dy;
                }

                var formSize = this.Size;
                formSize.Width += dx;
                formSize.Height += dy;
                
                Size = formSize;
                Location = formLocation;

                memoBoxMouseDownLocation = location;
            }

            Cursor cursorType = Cursor.Current;
            uint resizeFlag = GetCursorTypeFromLocation(this, e.Location, ref cursorType);

            if (Cursor.Current != cursorType)
                Cursor.Current = cursorType;

            formResizeFlag = resizeFlag;
        }

        private void memoForm_MouseUp(object sender, MouseEventArgs e)
        {
            formResizeFlag = 0x00;
            moveState = false;
            UpdateStyles();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void readOnlyButton_Click(object sender, EventArgs e)
        {
            memoBox.ReadOnly = !memoBox.ReadOnly;
            readOnlyButton.ForeColor = memoBox.ReadOnly ? Color.Black : Color.Transparent;
        }

        private void memoBox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            /*
            if (e.Control)
            {
                switch(e.KeyCode)
                {
                    case Keys.A:
                        memoBox.SelectAll();
                        break;
                    case Keys.Z:
                        memoBox.Undo();
                        break;
                    case Keys.Y:
                        memoBox.Redo();
                        break;
                    case Keys.C:
                        memoBox.Copy();
                        break;
                    case Keys.X:
                        memoBox.Cut();
                        break;
                    case Keys.V:
                        // 현재 Text 만 가능하도록, 나중에 좀 더 개선의 여지가 필요
                        memoBox.Paste(DataFormats.GetFormat(DataFormats.Text));
                        break;
                }
            }
            */
        }

        private void memoBox_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            // Text 의 url 을 클릭 했을 때
        }

        private void MemoForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            var am = AppManager.Instance;
            Debug.Assert(am != null);

            am.UpdateMemoList(memoData);
            am.ClosingMemoWindow(MemoData);
        }

        private void memoBox_TextChanged(object sender, EventArgs e)
        {
            if (MemoData != null && enableModified)
            {
                MemoData.modifiedTime = DateTime.Now;
                MemoData.saved = false;
            }
        }

        private void OnPaintTextBox(PaintEventArgs e)
        {
            var am = AppManager.Instance;
            if (am != null)
            {
                var bm = am.memoBackgroudImage;
                DrawBackgroundImage(memoBox, bm, e);
            }
        }

        private void DrawBackgroundImage(Control traget, Bitmap bm, PaintEventArgs e)
        {
            // 잠시 사용하지 않음
            const int _scaleType = 1;

            if (bm != null)
            {
                float vf = (float)traget.Height / (float)bm.Height;
                float hf = (float)traget.Width / (float)bm.Width;

                float f;
                if (_scaleType == 1)
                {
                    f = vf > hf ? vf : hf;
                }
                //else
                //{
                //    f = vf < hf ? vf : hf;
                //}

                int width = (int)(bm.Width * f);
                int height = (int)(bm.Height * f);

                int x = (traget.Width - width) / 2;
                int y = (traget.Height - height) / 2;

                var canvas = e.Graphics;
                canvas.DrawImage(bm, x, y, width, height);
            }
        }

        private void MemoForm_Deactivate(object sender, EventArgs e)
        {
            var am = AppManager.Instance;
            if (am != null)
                am.MemoFormDeactive(this);
        }
    }
}
