﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace StickyNote
{
    public partial class ListForm : Form
    {
        public ListForm()
        {
            InitializeComponent();
        }

        private void ListForm_Load(object sender, EventArgs e)
        {
            var listPopupMenu = new ContextMenu();
            listPopupMenu.MenuItems.Add("&New", OnNewMemo);
            listPopupMenu.MenuItems.Add("&Show", OnShowMenu);
            listPopupMenu.MenuItems.Add("&Close", OnCloseMenu);
            listPopupMenu.MenuItems.Add("&Delete", OnDeleteMenu);

            memoListView.ContextMenu = listPopupMenu;

            InitListBox();
            RefreshListBox();
        }

        void OnNewMemo(object sender, EventArgs e)
        {
            var form = AppManager.Instance.CreateNewMemoWindow();
        }

        void OnShowMenu(object sender, EventArgs e)
        {
            var am = AppManager.Instance;
            if (am != null)
            {
                foreach (ListViewItem i in memoListView.SelectedItems)
                {
                    var key = i.Tag.ToString();
                    am.ShowMemoWindow(key, true);
                }
            }
        }

        void OnCloseMenu(object sender, EventArgs e)
        {
            var am = AppManager.Instance;
            if (am != null)
            {
                foreach (ListViewItem i in memoListView.SelectedItems)
                {
                    var key = i.Tag.ToString();
                    am.CloseMemoWindow(key);
                }
            }
        }

        void OnDeleteMenu(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Delete memos", "!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                var am = AppManager.Instance;
                Debug.Assert(am != null);

                foreach (ListViewItem item in memoListView.SelectedItems)
                {
                    string key = item.Tag.ToString();
                    
                    am.RemoveMemo(key, false);
                    memoListView.Items.Remove(item);
                }
            }
        }

        //private void memoListBox_DrawItem(object sender, DrawItemEventArgs e)
        //{
        //    e.DrawBackground();
        //    e.DrawFocusRectangle();
        //    e.Graphics.DrawString(memoListBox.Items[e.Index].ToString(), this.Font, Brushes.Black, e.Bounds);
        //}

        private void InitListBox()
        {
            memoListView.GridLines = false;
            memoListView.FullRowSelect = true;
            memoListView.View = View.Details;

            memoListView.Columns.Clear();
            memoListView.Columns.Add("", 10);
            memoListView.Columns.Add("Title", 150);
            memoListView.Columns.Add("Created", 138);
            memoListView.Columns.Add("Modified", 138);
        }

        public void RefreshListBox()
        {
            var am = AppManager.Instance;
            if (am != null)
            {
                memoListView.Items.Clear();
                foreach (var v in am.MemoTable.Values)
                {
                    AddMemoListItem(v);
                }
            }
        }

        private ListViewItem SetupListViewItem(ListViewItem item, MyMemoData data)
        {
            Debug.Assert(item != null && data != null);

            item.SubItems.Clear();
            item.SubItems.Add(data.titleText);
            item.SubItems.Add(data.createdTime.ToDateString());
            item.SubItems.Add(data.modifiedTime.ToDateString());

            item.Text = string.Empty;

            return item;
        }

        private ListViewItem FindItemByKey(string key)
        {
            foreach (var i in memoListView.Items)
            {
                var item = i as ListViewItem;
                if (item != null)
                {
                    var itemKey = item.Tag.ToString();
                    if (itemKey.Equals(key))
                        return item;
                }
            }

            return null;
        }

        public void AddMemoListItem(MyMemoData data)
        {
            ListViewItem item = SetupListViewItem(new ListViewItem(), data);

            item.Tag = data.key;
            memoListView.Items.Add(item);
        }

        public void RemoveMemoListItem(string key)
        {
            var item = FindItemByKey(key);
            if (item != null)
            {
                memoListView.Items.Remove(item);
            }
        }

        public void UpdateDataListItem(MyMemoData memoData)
        {
            var item = FindItemByKey(memoData.key);
            if (item != null)
            {
                SetupListViewItem(item, memoData);
            }
        }

        private void ListForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            var am = AppManager.Instance;
            if (am != null)
            {
                am.CloseListWindow();
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            var am = AppManager.Instance;
            if (am != null)
            {
                foreach (ListViewItem i in memoListView.SelectedItems)
                {
                    var key = i.Tag.ToString();
                    am.SaveMemoDataFromKey(key);
                }
            }
        }

        private void memoListView_DoubleClick(object sender, EventArgs e)
        {
            OnShowMenu(sender, e);
        }
    }
}
