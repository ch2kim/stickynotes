﻿using System;
using System.Drawing;
using System.Windows.Forms;

// 전역 단축키를 위해 MainForm 을 만듦.
namespace StickyNote
{
    public partial class MainForm : Form
    {
        private AppManager appManager = null;
        private NotifyIcon notifyIcon;

        public MainForm()
        {
            InitializeComponent();

            appManager = new AppManager();
            appManager.Init(this);

            Init();
        }

        private void Init()
        {
            MenuItem newMemoMenuItem = new MenuItem("&New Memo", OnNewMemo);
            MenuItem listMenuItem = new MenuItem("Memo &List", OnListWindow);
            MenuItem settingMenuItem = new MenuItem("&Setting", OnSettingWindow);
            MenuItem exitMenuItem = new MenuItem("E&xit", OnExitMenu);

            notifyIcon = new NotifyIcon();
            notifyIcon.Icon = Properties.Resources.TrayIcon;
            notifyIcon.ContextMenu = new ContextMenu(new MenuItem[]
                {
                        newMemoMenuItem,
                        listMenuItem,
                        settingMenuItem,
                        new MenuItem("-"),
                        exitMenuItem,
                });

            notifyIcon.Visible = true;
        }

        private void Quit()
        {
            // Hide tray icon, otherwise it will remain shown until user mouses over it
            if (notifyIcon != null)
            {
                notifyIcon.Visible = false;
                notifyIcon.Dispose();
                notifyIcon = null;
            }

            AppManager.Instance.Quit();
        }

        void OnNewMemo(object sender, EventArgs e)
        {
            var f = AppManager.Instance.CreateNewMemoWindow();
        }

        void OnSettingWindow(object sender, EventArgs e)
        {
            AppManager.Instance.ShowSettingWindow();
        }

        void OnListWindow(object sender, EventArgs e)
        {
            AppManager.Instance.ShowListWindow();
        }

        public void OnExitMenu(object sender, EventArgs e)
        {
            var mainWindow = AppManager.Instance.MainWindow;
            if (mainWindow != null)
            {
                mainWindow.Close();
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Quit();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Size = new Size(2, 2);
            WindowState = FormWindowState.Minimized;

            Hide();
        }
    }
}
