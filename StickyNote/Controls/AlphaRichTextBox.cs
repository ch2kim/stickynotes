﻿using System.Windows.Forms;

namespace StickyNote.Controls
{
    // https://stackoverflow.com/questions/4910036/transparent-richtextbox
    class AlphaRichTextBox : TextBox
    {
        public System.Action<PaintEventArgs> OnPaintHandle = null;

        protected override CreateParams CreateParams
        {
            get
            {
                //This makes the control's background transparent
                CreateParams CP = base.CreateParams;
                //CP.ExStyle |= 0x20;
                CP.ExStyle |= 0x00020000; // STATICEDGE
                return CP;
            }
        }

        protected override void OnPrint(PaintEventArgs e)
        {
            base.OnPrint(e);

            OnPaintHandle?.Invoke(e);
        }
    }
}
