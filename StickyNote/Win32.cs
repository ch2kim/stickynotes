﻿using System;
using System.Runtime.InteropServices;

namespace StickyNote
{
    class Win32
    {

        public const Int32 GWL_EXSTYLE = -20;
        public const Int32 WS_EX_LAYERED = 0x00080000;
        public const Int32 WS_EX_TRANSPARENT = 0x20;
        public const Int32 LWA_ALPHA = 0x00000002;

        // 출처: https://slaner.tistory.com/37 [꿈꾸는 프로그래머]

        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);

        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        [DllImport("user32")]
        public static extern Int32 SetWindowLong(IntPtr hWnd, Int32 nIndex, Int32 dwNewLong);

        [DllImport("user32")]
        public static extern Int32 GetWindowLong(IntPtr hWnd, Int32 nIndex);

        [DllImport("user32.dll")]
        public static extern bool SetLayeredWindowAttributes(IntPtr hWnd, UInt32 crKey, byte bAlpha, UInt32 dwFlags);
    }
}
