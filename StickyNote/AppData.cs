﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace StickyNote
{
    public class MyMemoData
    {
        public string key;

        public Point memoLocation;
        public Size memoSize;
        public bool show;

        public DateTime createdTime;
        public DateTime modifiedTime;

        public string titleText = null;
        public string memoText = null;

        [NonSerialized]
        public FormWindowState memoState;

        [NonSerialized]
        public bool saved = false;

        [NonSerialized]
        public MemoForm memoForm = null;
    }
}
