﻿using System.Windows.Forms;

namespace StickyNote
{
    public partial class InputTextDialog : Form
    {
        public InputTextDialog()
        {
            InitializeComponent();
        }

        public string InputText
        {
            get
            {
                return inputTextBox.Text;
            }
            set
            {
                inputTextBox.Text = value;
            }
        }
    }
}
